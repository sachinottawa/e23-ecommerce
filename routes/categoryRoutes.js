const express = require('express')
const { getAllCategories, getCategoryById, updateCategory, deleteCategory } = require('../controller/categoryController')
const router = express.Router()

// Get all categories - GET
router.get('/', getAllCategories)

// Get category by ID - GET
router.get('/:categoryId', getCategoryById)

// Update category
router.patch('/:categoryId', updateCategory)

// Delete category
router.delete('/:categoryId', deleteCategory)

module.exports = router