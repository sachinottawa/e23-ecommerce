const express = require('express')
const { getAllProducts, getProductById, addProduct, updateProduct, deleteProduct } = require('../controller/productController')
const router = express.Router()

// Get all products - GET
router.get('/products', getAllProducts)

// Get product by Id - GET
router.get('/products/:productId', getProductById)

// Add new product - POST
router.post('/products', addProduct)

// Udpate product - PATCH
router.patch('/products/:productId', updateProduct)

// Delete one product - DELETE
router.delete('/products/:productId', deleteProduct)

module.exports = router